package com.martinez.app.interactor;

import com.martinez.app.interfaz.iteractor.I_mainIntereactor;
import com.martinez.app.interfaz.modelo.Info;
import com.martinez.app.interfaz.presentador.I_mainPresenter;
import com.martinez.app.presenter.mainPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan Martinez on 25/09/16.
 */

public class mainInteractor implements I_mainIntereactor {
    private I_mainPresenter i_mainPresenter;
    public mainInteractor(mainPresenter mainPresenter) {
        this.i_mainPresenter = mainPresenter;
    }

    @Override
    public void startRecycler() {
        List<Info> infoList = new ArrayList<>();
        infoList.add(new Info("Llanero","centro comercial","av. Lara"));
        infoList.add(new Info("CoffeCoke","dulceria","av. leones"));
        i_mainPresenter.startRecycler(infoList);
    }
}
