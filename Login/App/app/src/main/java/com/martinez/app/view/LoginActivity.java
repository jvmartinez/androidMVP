package com.martinez.app.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.martinez.app.R;
import com.martinez.app.interfaz.presentador.I_LoginPresenter;
import com.martinez.app.interfaz.view.I_login;
import com.martinez.app.presenter.loginPresenter;

public class LoginActivity extends AppCompatActivity implements I_login, View.OnClickListener {
    private EditText txtUser;
    private EditText txtPassword;
    private Button btnButton;
    private ProgressBar progressBar;
    private I_LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.txtUser = (EditText) findViewById(R.id.txtUser);
        this.txtPassword = (EditText) findViewById(R.id.txtPassword);
        this.btnButton = (Button) findViewById(R.id.btnSuccess);
        this.progressBar =(ProgressBar) findViewById(R.id.progressBar);
        this.btnButton.setOnClickListener(this);


        this.loginPresenter = new loginPresenter(this);
    }



    @Override
    public void showUser() {
        this.txtUser.setError("*");
    }

    @Override
    public void showPassword() {
        this.txtPassword.setError("*");
    }

    @Override
    public void showinfoIncorrect() {
        this.txtUser.setError("Intente otra vez");
    }

    @Override
    public void hideProgressbar() {
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showProgressbar() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void navegation() {
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        this.loginPresenter.verifyLogin(txtUser.getText().toString(),txtPassword.getText().toString(),getBaseContext());
    }
}
