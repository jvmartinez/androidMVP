package com.martinez.app.interfaz.view;

/**
 * Created by Juan Martinez on 13/09/16.
 */
public interface I_login {
    void showUser();
    void showPassword();
    void showinfoIncorrect();
    void hideProgressbar();
    void showProgressbar();
    void navegation();
}
