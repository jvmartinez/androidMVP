package com.martinez.app.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.martinez.app.R;
import com.martinez.app.adapter.infoAdapter;
import com.martinez.app.interfaz.modelo.Info;
import com.martinez.app.interfaz.presentador.I_mainPresenter;
import com.martinez.app.interfaz.view.I_main;
import com.martinez.app.presenter.mainPresenter;

import java.util.List;

public class MainActivity extends AppCompatActivity  implements I_main{
    private RecyclerView recyclerView;
    private infoAdapter adapter;
    private I_mainPresenter iMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iMainPresenter = new mainPresenter(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerContent);
        adapter = new infoAdapter(R.layout.activity_custom_home);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        iMainPresenter.loadInfo();
    }

    @Override
    public void showData() {

    }

    @Override
    public void showRecycle(List<Info> infoList) {
        adapter.setInfoListInfo(infoList);
    }
}
