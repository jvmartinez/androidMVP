package com.martinez.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martinez.app.R;
import com.martinez.app.interfaz.modelo.Info;

import java.util.List;

/**
 * Created by Juan Martinez on 25/09/16.
 */

public class infoAdapter extends RecyclerView.Adapter<infoAdapter.ViewHolder> implements View.OnClickListener {
    private List<Info> infoList;
    private int itemLayout;
    public infoAdapter(int itemLayout){
        this.itemLayout = itemLayout;
    }

    public void setInfoListInfo(List<Info> infoListInfo){
        this.infoList = infoListInfo;
        notifyDataSetChanged();
    }
    public void add(Info info, int posi){
        this.infoList.add(posi,info);
    }
    public void remove(Info info){
        int index = this.infoList.indexOf(info);
        this.infoList.remove(index);
        notifyDataSetChanged();
    }
    @Override
    public infoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.itemLayout,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(infoAdapter.ViewHolder holder, int position) {
        final Info info = infoList.get(position);
        holder.itemView.setTag(info);
        holder.nombre.setText(info.getNombre());
        holder.descripcion.setText(info.getDescripcion());
    }

    @Override
    public int getItemCount() {
        if(this.infoList != null )
            return this.infoList.size();
        else
            return 0;
    }

    @Override
    public void onClick(View v) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nombre;
        private TextView descripcion;
        public ViewHolder(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.lblOpcion);
            descripcion = (TextView) itemView.findViewById(R.id.lbldescripcion);
        }
    }
}
