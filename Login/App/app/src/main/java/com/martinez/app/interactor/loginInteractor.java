package com.martinez.app.interactor;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.martinez.app.interfaz.iteractor.I_LoginIteractor;
import com.martinez.app.util.Constants;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Juan Martinez on 14/09/16.
 */

public class loginInteractor implements I_LoginIteractor {
    private String ok = "";
    public String varifyLogin(final String user, final String password, Context context) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ok = response.trim();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String,String> getParams() throws AuthFailureError{
                Map<String,String> map = new HashMap<>();
                map.put("username",user);
                map.put("password",password);
                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
        return this.ok;
    }
}
