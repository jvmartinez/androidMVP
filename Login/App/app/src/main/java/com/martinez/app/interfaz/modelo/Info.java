package com.martinez.app.interfaz.modelo;

/**
 * Created by Juan Martinez on 25/09/16.
 */

public class Info {
    private String nombre;
    private String descripcion;
    private String direccion;

    public Info(String nombre, String descripcion,String direccion){
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
