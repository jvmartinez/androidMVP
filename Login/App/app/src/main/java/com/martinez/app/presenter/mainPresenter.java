package com.martinez.app.presenter;

import com.martinez.app.interactor.mainInteractor;
import com.martinez.app.interfaz.iteractor.I_mainIntereactor;
import com.martinez.app.interfaz.modelo.Info;
import com.martinez.app.interfaz.presentador.I_mainPresenter;
import com.martinez.app.interfaz.view.I_main;

import java.util.List;

/**
 * Created by Juan Martinez on 25/09/16.
 */

public class mainPresenter implements I_mainPresenter {
    private I_main i_main;
    private I_mainIntereactor i_mainIntereactor;

    public mainPresenter(I_main i_main){
        this.i_main = i_main;
        i_mainIntereactor = new mainInteractor(this);
    }
    @Override
    public void startRecycler(List<Info> infoList) {
        this.i_main.showRecycle(infoList);
    }

    @Override
    public void loadInfo() {
        i_mainIntereactor.startRecycler();
    }
}
