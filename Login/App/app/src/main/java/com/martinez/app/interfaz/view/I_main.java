package com.martinez.app.interfaz.view;

import com.martinez.app.interfaz.modelo.Info;

import java.util.List;

/**
 * Created by Juan Martinez on 18/09/16.
 */
public interface I_main {
    void showData();
    void showRecycle(List<Info> infoList);
}
