package com.martinez.app.interfaz.presentador;

import com.martinez.app.interfaz.modelo.Info;

import java.util.List;

/**
 * Created by janu on 25/09/16.
 */

public interface I_mainPresenter {
    void startRecycler(List<Info> infoList);
    void loadInfo();
}
