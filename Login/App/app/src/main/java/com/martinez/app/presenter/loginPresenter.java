package com.martinez.app.presenter;

import android.content.Context;

import com.martinez.app.interactor.loginInteractor;
import com.martinez.app.interfaz.iteractor.I_LoginIteractor;
import com.martinez.app.interfaz.presentador.I_LoginPresenter;
import com.martinez.app.interfaz.view.I_login;
import com.martinez.app.util.Constants;

/**
 * Created by Juan Martinez on 14/09/16.
 */
public class loginPresenter  implements I_LoginPresenter{
    private I_login view;
    private I_LoginIteractor i_loginIteractor;
    public loginPresenter(I_login activity){
        this.view = activity;
        this.i_loginIteractor = new loginInteractor();
    }

    @Override
    public void verifyLogin(String user, String password, Context baseContext) {
        if(this.view != null && !user.equals("") && !password.equals("")){
            if(this.i_loginIteractor.varifyLogin(user, password, baseContext) != null){
                if (this.i_loginIteractor.varifyLogin(user, password, baseContext).equals(Constants.success)) {
                    this.view.showProgressbar();
                    this.view.navegation();
                }else if(this.i_loginIteractor.varifyLogin(user, password, baseContext).equals(Constants.failure)){
                    this.view.showinfoIncorrect();
                }
            }
        }else{
            this.view.showUser();
            this.view.showPassword();
        }
    }
}
