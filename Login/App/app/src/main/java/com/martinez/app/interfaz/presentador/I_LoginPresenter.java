package com.martinez.app.interfaz.presentador;

import android.content.Context;

/**
 * Created by Juan Martinez on 14/09/16.
 */
public interface I_LoginPresenter {
    void verifyLogin(String user, String password, Context baseContext);
}
